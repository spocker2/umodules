# uModules 0.1

** PUBLIC SERVICE ANNOUNCEMENT **
~~~
This entire project is still WIP. Even my ramblings on about how it works is. 
There will be changes, it might break your project, you might even wipe out your hard drive! 
YOU HAVE BEEN WARNED!
~~~


Thanks for your interest in uModules. This project has been created to fill a gap that I felt was present in the project management for Games and Applications built with Unity3D. I come from a heavy Java/C# coding background where we have tools at our disposal like Ant, Ivy and other build management systems.

uModules, is my crazy attempt to take some of those core ideas, specifically module management, code re-use along with predictable development and testing, and make them a core to my development workflow with Unity.

I must warn you, this is something to scratch my itch, and it won't be for everyone. However, I am putting it out here in hopes that people contribute and make the tool better.

## What is uModules

uModules is a script, written in Python (poorly), that lets you build your Unity project structure consistently and easily. It is NOT a script for building the final executeable of your App. Instead, it focuses on the core principal of modular development.

The script reads a YAML configuration files, that tells it what your main project is, and where every supporting module can be found.

## Modular Development

In most Unity projects, you will have your main set of Assets, your app, with any number of supporting Modules added. It may be the Standard Assets, it might be something you purchased on the Asset store, it may even be a custom set of scripts or objects that you have in another project.

uModules, will let you tell your Unity project which Modules need to be added to your structure, where they come from, and how they should be added to your project.

## Why Would I Want To Do This?

Modular development leads to better programming practices, allows you to share your code, and can reduce dependencies in your final app builds. It gives you peace of mind that when you are building for a specific platform that you will have everything you need, and not have things your do not need.

Let's say there is a new version of a Plugin on the Unity Asset Store that you really want to try out. Simply create a new folder, copy the uModules project.yml configuration file, download the new Asset from the store and run

~~~~
>py umodules.py build
~~~~

A few minutes later you entire project will be ready for you to open in Unity.

Of course, you could also try it in your existing project too

~~~~
>py umodules.py clean
>py umodules.py build
~~~~

This will clean out all of the Modules in your project, and then rebuild them.

## How Does It Work?

In principal it works around this concept. You have one Main project, and Many supporting projects. With uModules, you tell it where your Main project code is located, hopefully this is on a VCS somewhere, but it could be on a local drive, or another folder on your computer.

Here is an example

~~~~
# main project
main: 

 name: SmokeTestMain
 url: https://bitbucket.org/spocker2/smoketestmain.git
 branch: develop
~~~~

So what is happening here? First, the name of our Project is called SmokeTestMain, this will utimately be the folder that your project is built from. It can be in the same folder that you are running uModules from, or it can be in a completely different place on your local computer. By default, the system assumes that urls are from a GIT repository. This can of course be changed to use one of the other supported module types. When you are using a VCS like GIT, you can specify what BRANCH or TAG you might want to pull the code from.

When you run the uModules script, the tool will pull the source from the GIT repo using the name you specified, your project
will look something like this.

~~~~
SmokeTestMain
|
|-.git
|-.gitignore
|-Assets/
| |
| |-SmokeTestMain
| | |
| | |-SomeCoolClass.cs
| | |-...
|
|-ProjectSettings/
  |
  |-EditorSetting.asset
  |-...
~~~~

Next, let's say you found an Open Source Plugin that is available that will do Tweening, from a really cool developer. You want to add this module to your project. You simply need to tell the uModules configuration the following:

~~~~
- module: SomeModule
  url : http://somecoolwebsite.com/SomModule.zip
  type: zip
  active: True
~~~~

What will happen when the script sees this is it will, download the zip file from the url specified and unzip it into a local repository. Second, it will copy the entire folder to the project folder defined in your configuration file. When it is done, the project would look something like this.

~~~~
SmokeTestMain
|
|-.git
|-.gitignore
|-Assets/
| |
| |-SmokeTestMain
| | |
| | |-SomeCoolClass.cs
| | |-...
| |-SomeModule
| | |
| | -ReallyCoolTween.cs
|
|-ProjectSettings/
  |
  |-EditorSetting.asset
  |-...
~~~~

This just scratches the surface. The tool will let you pull specific folders from the module you download, change the default location, etc. There will be more examples as I continue to work on this.

## What is a Real World example of This?

The game [Kozmic Blue](http://kozmicbluegame.com) has been built for the most part with this Tool. More specifically, the tool has been created around this project. The game is a multi platform game that has different modules needed for different platforms.

For example, a custom Cryptology DLL needed to be built for the Windows App Store (Metro). In addition the Metro build needed a special Mico version of the HOTween library. Using the tool I was able to have one Project for Metro, one for Androind and one for iOS, each with slightly different Plugins and code addded. They all used the Main project as its core. When updates were made to the Main project, they were simply pulled down into the respective Platform project and the App was built in Untiy for testing.

## Are There Any Risks?

As with everything in life, there are Risks? To date, I have not had any significant problems. I am using many different plugins with Prefabs, and objects, and I have yet to encounter any show stoppers.

## What You Need

Python 3.4 (don't think will work with 2.x) with the following:

*subprocess
*yaml
*argparse
*shutil

## How To Use

python.exe umodule.py --help
python.exe umodule.py build (will build from project.yml)
python.exe umodule.py cleanall (will remove all project objects from in project.yml)


* if you have assets from the unity store, place them in a central location and change the "unity_packages" parameter
*change the "unity_path" to where your unity exe is located, this is a good way to make sure you are importing using the correct version across platforms, etc
* default type is GIT
* default GIT branch = MASTER

Currently supported types are:

* GIT = from and GIT repo like github, bitbucket, etc (this will do a git pull from repo)
* UNITY_PACKAGE = local copy of unitypackage from asset store - will open unity and import each package

Planned supported types are:

* ZIP = download from http and unzip, or copy from local repo
* SVN = from any svn repo
* NEXUS = from nexus repo