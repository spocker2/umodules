"""
The MIT License (MIT)

Copyright (c) 2014 SpockerDotNet LLC (http://sdnllc.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

import subprocess
import argparse
import shutil
import os
import stat
import platform
import logging
import collections
import zipfile
import re
import yaml
import wget

from distutils.dir_util import copy_tree
from enum import Enum
from urllib.parse import urlparse


class Module(object):
    def __init__(self, name, url, branch, module_type, include, active):
        self.name = name
        self.url = url
        self.branch = branch
        self.type = module_type
        self.active = active
        self.include = include


class MainModule(Module):
    def __init__(self, name, url, branch, module_type):
        self.name = name
        self.url = url
        self.branch = branch
        self.type = module_type
        self.include = []
        self.modules = []


class Project(object):
    def __init__(self, name, path, unity_path, unity_packages, default_type):
        self.name = name
        self.path = path
        self.unityPath = replace_var(unity_path)
        self.unityPackages = replace_var(unity_packages)
        self.defaultType = default_type
        self.main = None


class Include(object):
    def __init__(self):
        self.dir = "Assets/"
        self.todir = "Assets/"


class ModuleTypes(str, Enum):
    git = "git"
    unity_package = "unity_package"
    unity_package_file = "unity_package_file"
    unity_package_http = "unity_package_http"
    nexus_repo = "nexus_repo"
    svn = "svn"
    http = "http"
    local = "local"
    file = "file"
    zip = "zip"


def add_argument_modules(arg):
    arg.add_argument("modules", action="store", nargs="*")


def _copy_tree(src, dst):
    logger.debug("*** _copy_tree")
    logger.debug("src = {0}".format(src))
    logger.debug("src = {0}".format(dst))
    if os.path.isdir(src):
        if not os.path.exists(dst):
            os.makedirs(dst)
        for name in os.listdir(src):
            _copy_tree(os.path.join(src, name), os.path.join(dst, name))
    else:
        shutil.copyfile(src, dst)


def xcopy_tree(dsrc, ddst):
    logger.debug("! copy_tree")
    logger.debug("src = {0}".format(dsrc))
    logger.debug("src = {0}".format(ddst))
    for dirname in os.listdir(dsrc):
        tocopy = os.path.join(dsrc, dirname)
        for d in os.listdir(tocopy):
            src = os.path.join(tocopy, d)
            dst = os.path.join(ddst, d)
            if os.path.isdir(src):
                _copy_tree(src, dst)


def remove_readonly(func, path, exc_info):
    try:
        logger.debug(exc_info)
        os.chmod(path, stat.S_IWRITE)
        func(path)
    except:
        pass


def remove_main():
    logger.debug("*** remove_main()")
    p = "{0}/".format(project.name)
    p = os.path.abspath(p)
    logger.debug(p)

    try:
        print("removing project {0}".format(project.name), end="")
        shutil.rmtree(p, onerror=remove_readonly)
        print(" (Done)")
    except OSError as e:
        print(" (Error)")
        print(e)

    r = ".repo/{0}".format(project.name)
    r = os.path.abspath(r)
    logger.debug(r)

    try:
        print("removing project {0} repository".format(project.name), end="")
        shutil.rmtree(r, onerror=remove_readonly)
        print(" (Done)")
    except OSError as e:
        print(" (Error)")
        print(e)


def remove_module(module):
    logger.debug("*** remove_module()")

    print("Removing Module {0} ".format(module.name), end="")

    mod = "{1}/Assets/{0}".format(module.name, project.name)
    src = ".repo/{1}/{0}".format(module.name, project.name)

    try:
        logger.debug("removing module {1} from project {0}".format(mod, module.name))
        shutil.rmtree(mod, onerror=remove_readonly)
    except Exception as e:
        print("(Error)")
        raise e
    try:
        logger.debug("removing module {1} from repository {0}".format(src, project.main.name))
        shutil.rmtree(src, onerror=remove_readonly)
    except Exception as e:
        print("(Error)")
        raise e

    try:
        logger.debug("removing module {0} meta data".format(module.name))
        meta = "{1}/Assets/{0}.meta".format(module.name, project.main.name)
        if os.path.exists(meta):
            os.remove(meta)
    except Exception as e:
        print("(Error)")
        raise e

    print("(Done)")


def clean():
    """Clean all Modules

    This will Remove all Modules from the Project and
    from the Repository

    """
    logger.debug("*** clean()")
    clean_modules()


def clean_all():
    """Clean Entire Project

    This will Remove everything from your Project
    and Repository.

    *** Use with Care ***

    """
    logger.debug("*** clean_all()")

    sts = status_main()

    if sts == 1:
        print("")
        print("** WARNING - MAIN PROJECT BEHIND **")
        print("")
        print("Your Main project is Behind. This is usually")
        print("okay, but to be safe, are you sure you want to Clean?")
        print("")
        yes = input("Enter YES to continue: ")
        if yes == "YES":
            pass
        else:
            print()
            print("** Clean Cancelled **")
            return

    if sts == 2:
        print("")
        print("** WARNING - MAIN PROJECT MODIFIED **")
        print("")
        print("Your Main project has been Modified. Are you sure")
        print("you want to Clean?")
        print("")
        yes = input("Enter YES to continue: ")
        if yes == "YES":
            pass
        else:
            print()
            print("** Clean Cancelled **")
            return

    if sts == 3:
        print()
        print("** Clean Cancelled **")
        return

    print("")
    print("** WARNING **")
    print("")
    print("This will remove EVERYTHING from this project. Please make")
    print("sure you have pushed your work before you continue!!")
    print("")
    yes = input("Enter YES to continue: ")
    if yes == "YES":
        clean_modules()
        clean_main()
    else:
        print()
        print("** Clean Cancelled **")
        return


def clean_main():
    logger.debug("*** clean_main()")
    remove_main()


def clean_modules():
    logger.debug("*** clean_modules()")
    for m in project.main.modules:
        remove_module(m)


def edit():
    """Open Unity for Editing the Project

    """
    logger.debug("*** edit()")

    try:
        p = get_unity_process()
        logger.debug(p)
        proc = subprocess.Popen(p, shell=True)
        proc.communicate()
    except Exception as e:
        print(e)


def get_unity_process(run=True):
    """Create a Command Line Process for Running Unity

    This function will return a process string that
    can be used to run Unity on Windows or Mac.

    Unsupported platforms will raise an Exception.

    :return: process as str
    """
    logger.debug("*** get_unity_process()")

    p = ""

    q = ""

    # unity will fail to import any packages in batchmode if there are compile errors, turning that off for now
    if quiet:
        # q = "-batchmode -nographics"
        q = ""

    # default Unity executable is Windows
    if platform.system() == "Windows":
        logger.info("running on Windows")
        unity = "Unity.exe"
        unity = os.path.abspath(project.unityPath + unity)
        logger.debug(unity)
        pp = os.path.abspath(project.name)
        logger.debug(pp)
        r = ""
        if run:
            r = "start /B \"Unity\""
        p = "{0} \"{1}\" -projectPath \"{2}\"".format(r, unity, pp)
        logger.debug(p)
        np = "{0} {1}".format(p, q)
        logger.debug(np)
        return np

    # override if we are running Mac
    if platform.system() == "Darwin":
        logger.info("running on Mac")
        unity = "Unity.app/Contents/MacOS/Unity"
        unity = os.path.abspath(project.unityPath + unity)
        pp = os.path.abspath(project.name)
        logger.debug(pp)
        r = ""
        if run:
            r = "&"
        p = "\"{0}\" -projectPath \"{1}\" {2}".format(unity, pp, r)
        logger.debug(p)
        np = "{0} {1}".format(p, q)
        logger.debug(np)
        return np

    raise("Unable to Run Unity on {0} Platform".format(platform.system()))


def get_unity_cli():
    """Return a Simple Unity command line

    """
    logger.debug("*** get_unity_cli()")

    # default Unity executable is Windows
    if platform.system() == "Windows":
        logger.info("running on Windows")
        unity = "Unity.exe"
        unity = os.path.abspath(project.unityPath + unity)
        logger.debug(unity)
        p = "\"{0}\"".format(unity)
        logger.debug(p)
        return p

    raise Exception("Unable to Run Unity on {0} Platform".format(platform.system()))


def update():
    global force
    force = True
    pull_modules()


def push():
    print("push")


def check_module_status(module, git_fetch, git_status):

        m = module

        # print(git_fetch)
        # print(git_status)

        try:
            fetch = subprocess.Popen(git_fetch, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            fetch.communicate()
        except Exception as e:
            print("Could not fetch latest updates from ")
            print(e)

        try:
            print("Checking status of {1}/{0} ".format(m.name, project.name), end="")
            sts = subprocess.Popen(git_status, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            sout, serr = sts.communicate()
            ret = sout.decode("utf-8")
            err = serr.decode("utf-8")
            if len(err) <= 0:
                if ret.find("modified") > 0:
                    print("(Modified)")
                else:
                    if ret.find("behind") > 0:
                        print("(Behind)")
                    if ret.find("up-to-date") > 0:
                        print("(Up To Date)")
            else:
                print("(Error)")
        except Exception as e:
            print("Could not get the Status of Module {0}".format(m.name))
            print(e)


def status():
    status_main()
    status_modules()


def status_main():

    print("Status of your Main project {0} is ".format(project.name), end="")

    if project.main.type == "git":
        sts = status_git(project.name)
        return sts

    print("(Unknown)")


def status_modules():
    """Check the Status of All Modules in the Repository

    This function will check the Status of a Module so
    that the Developer can quickly see if something needs
    to refreshed in the project.

    """
    for module in project.main.modules:
        if module.active:
            print("Status of Module {0} is ".format(module.name), end="")

            if module.type == ModuleTypes.git:
                status_git(".repo/{0}/{1}".format(project.name, module.name))
                continue

            print("(Unsupported)")


def status_git(tree):
    """Check the Status of a Project on a Git Repository

    This function will Fetch the latest version from
    a Repository and then Print the Status to the console.


    :return:
    """
    git_dir = os.path.abspath(tree + "/.git")
    work_dir = os.path.abspath(tree)

    logger.debug("tree = " + tree)
    logger.debug("git dir = " + git_dir)
    logger.debug("work dir = " + work_dir)

    git_fetch = "git --git-dir={0} --work-tree={1} fetch".format(git_dir, work_dir)
    git_status = "git --git-dir={0} --work-tree={1} status".format(git_dir, work_dir)

    logger.debug("git fetch = " + git_fetch)
    logger.debug("git status = " + git_status)

    try:
        fetch = subprocess.Popen(git_fetch, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        fetch.communicate()
    except Exception as e:
        print()
        print("Could not fetch latest updates from ")
        print(e)

    try:
        sts = subprocess.Popen(git_status, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        sout, serr = sts.communicate()
        ret = sout.decode("utf-8")
        logger.debug(ret)
        err = serr.decode("utf-8")
        logger.debug(err)
        r = 0
        s = "(Error)"
        if len(err) <= 0:
            if ret.find("up-to-date") > 0:
                s = "(Up To Date)"
                r = 0
            if ret.find("behind") > 0:
                s = "(Behind)"
                r = 1
            if ret.find("Untracked") > 0:
                s = "(Modified)"
                r = 2
            if ret.find("modified") > 0:
                s = "(Modified)"
                r = 2
            print(s)
            return r
        else:
            print("(Error)")
            return 3

    except Exception as e:
        print()
        print("Could not get the Status from Git")
        print(e)


def pull():
    """Pull Project Source and Modules

    """
    logger.debug("*** pull()")

    p = os.path.abspath(project.name)
    logger.debug(p)

    # if we have not specified any specific modules then skip pulling main project
    if len(modules) == 0:
        pull_main()

    pull_modules()


def pull_main():
    """Create the Project Folder and Repository

    This function will check and see if the Project
    and Repository Folders exist in the current path,
    and if they do not, will create them for us.

    If the Main Project already exists, then we simply
    skip over it.

    """
    logger.debug("*** pull_main()")
    print("Trying to Pull Main project {0} ".format(project.main.name), end="")

    git = "git clone -b {0} {1} {2}".format(project.main.branch, project.main.url, project.name)
    logger.debug('> ' + git)
    proc = subprocess.Popen(git, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    proc.communicate()

    # check if the repo path exists, if not create it
    repo = ".repo/{0}".format(project.name)
    logger.debug("checking to see if the repo path {0} exists".format(repo))
    if not os.path.exists(repo):
        os.makedirs(repo)
        logger.info("created the repo path {0}".format(repo))

    # check if the project path exists, if not create it
    pn = os.path.abspath(project.name)
    logger.debug("checking to see if the project path {0} exists".format(pn))
    if not os.path.exists(pn):
        os.makedirs(pn)
        logger.info("created the path {0} for project {1}".format(pn, project.name))

    print("(Done)")


def pull_modules():
    """Pull Modules Into the Project

    This function will Loop through each Active
    Module and pull it into the Project.

    If the developer asked for a specific module or
    modules, then just pull those individually.

    """
    logger.debug("*** pull_modules()")
    for module in project.main.modules:
        if module.active:
            try:
                if len(modules) > 0:
                    for name in modules:
                        if module.name.lower() == name.lower():
                            pull_module(module)
                else:
                    pull_module(module)
            except Exception as e:
                raise e


def pull_module(module):
    """Pulls a Module by Type

    This function determines what Type of Module
    to pull, and then call the proper function
    to handle it.

    :param module: the Module to pull
    """
    logger.debug("*** pull_module()")
    logger.debug("module: {0}".format(module.name))
    logger.debug(" type is {0}".format(module.type))
    logger.debug(" url is {0}".format(module.url))
    logger.debug(" branch is {0}".format(module.branch))
    logger.debug(" active is {0}".format(module.active))

    r = ".repo/{0}/{1}".format(project.name, module.name)
    r = os.path.abspath(r)
    logger.debug(r)

    # print(1)
    if os.path.exists(r):
        logger.debug("path already exists")

        if force:
            logger.debug("using the force")
            remove_module(module)
        else:
            print("Trying to Pull Module {0} ".format(module.name), end="")
            print("but it already exists (Skipping). Try using the --force.".format(module.name))
            return

    print("Trying to Pull Module {0} ".format(module.name), end="")

    if ModuleTypes[module.type] == ModuleTypes.git:
        logger.info("preparing to add git module {0}".format(module.name))
        pull_module_git(module)
        copy_includes(module)
        print("(Done)")
        return

    if ModuleTypes[module.type] == ModuleTypes.unity_package:
        print()
        logger.info("preparing to add unity_package module {0}".format(module.name))
        pull_module_package(module)
        return

    if ModuleTypes[module.type] == ModuleTypes.unity_package_file:
        print()
        logger.info("preparing to add unity_package_file module {0}".format(module.name))
        pull_module_package(module)
        return

    if ModuleTypes[module.type] == ModuleTypes.unity_package_http:
        print()
        logger.info("preparing to add unity_package_http module {0}".format(module.name))
        pull_module_package(module)
        return

    if ModuleTypes[module.type] == ModuleTypes.zip:
        logger.info("preparing to add zip module {0}".format(module.name))
        pull_module_zip(module)
        copy_includes(module)
        print("(Done)")
        return

    if ModuleTypes[module.type] == ModuleTypes.local:
        logger.info("preparing to add local module {0}".format(module.name))
        pull_module_local(module)
        copy_includes(module)
        print("(Done)")
        return

    print("did not recognize module type {0}".format(module.type))


def pull_module_git(module):
    """Pull a Module from Git

    This function will pull a Module from a Git
    Repository.

    If the Module is already in the Repository
    it will be skipped.

    :param module: the Module to pull from Git
    :return:
    """
    logger.debug("*** pull_module_git()")

    r = ".repo/{0}/{1}".format(project.name, module.name)
    r = os.path.abspath(r)
    logger.debug(r)

    if not os.path.exists(r):
        g = "git clone -b {0} {1} {2}".format(module.branch, module.url, r)
        logger.debug(g)
        proc = subprocess.Popen(g, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        proc.communicate()
    else:
        print("(Module Exists)(Skipping). Try using the --force.".format(module.name))


def pull_module_local(module):
    """Pull a Module from a Local source.

    Local Modules usually exist in a location relative to
    the Main project.

    :param module: The module object
    :return:
    """
    logger.debug("*** pull_module_local()")

    repo = '.repo/{0}/{1}'.format(project.name, module.name)
    repo = os.path.abspath(repo)

    src = os.path.abspath(urlparse(module.url).path)
    # dest = os.path.abspath(urlparse(repo).path)

    if os.path.exists(src):
        copy_tree(src, repo)
    else:
        logger.debug('unable to find local module {0} at location {1}'.format(module.name, src))
        print("(Error)")
        raise Exception("Local Module {0} was Not Found.".format(module.name))


def pull_module_package(module):

    unity = get_unity_cli()
    logger.debug(unity)
    pd = ".repo/{0}/{1}".format(project.name, module.name)
    logger.debug(pd)

    # default is http

    url = module.url

    if module.type == ModuleTypes.unity_package_file:
        url = "file:\\" + project.unityPackages + url

    logger.debug("url = " + url)

    os.makedirs(pd)
    package = "{0}/{1}/{2}.unitypackage".format(os.getcwd(), pd, module.name)
    logger.debug(package)
    download_file(url, package)

    q = ""
    if quiet:
        q = "-batchmode"

    ip = "-importPackage \"{0}\"".format(package)
    logger.debug(ip)
    p = "{0} -createProject {1} {2} {3} -quit".format(unity, pd, ip, q)
    logger.debug(p)

    proc = subprocess.Popen(p, shell=True)
    proc.communicate()
    print()

    src = "{0}/Assets".format(pd)
    src = os.path.abspath(src)
    logger.debug(src)

    dst = "{0}/Assets".format(project.name)
    dst = os.path.abspath(dst)
    logger.debug(dst)

    copy_tree(src, dst)


def pull_module_zip(module):
    """

    :param module:
    :return:
    """
    logger.debug("[umodules] get_module_zip")
    zipdir = ".repo/{3}/{1}".format(module.url, module.name, module.branch, project.name)
    if not os.path.exists(zipdir):
        cwd = os.getcwd()
        file = module.url
        outdir = "{1}/.repo/{2}/{0}".format(module.name, cwd, project.name)
        dest = "tmp.zip".format(outdir, module.name)
        os.makedirs(outdir)
        download_file(file, dest)
        with zipfile.ZipFile(dest) as myzip:
            myzip.extractall(path=outdir)
        os.remove(dest)
    else:
        print("The Module {0} has already been added (Skipping)".format(module.name))


def copy_includes(module):
    logger.debug("*** copy_includes()")
    for i in module.include:
        src = ".repo/{2}/{0}/{1}".format(module.name, i.dir, project.name)
        src = os.path.abspath(src)
        logger.debug(src)

        dst = "{0}/{1}".format(project.name, i.todir)
        dst = os.path.abspath(dst)
        logger.debug(dst)

        try:
            # shutil.copytree(src,dst)
            copy_tree(src, dst)
        except Exception as e:
            print("could not include {0} from {1}".format(i.todir, i.dir))
            print(e)


def download_file(url, dest):
    try:
        logger.debug(url)
        logger.debug(dest)
        wget.download(url, out=dest)
    except Exception as e:
        print("(Error)")
        raise e


def load_config():
    try:
        logger.debug("attempting to opening config file [{0}]".format(args.config))

        if not os.path.exists(args.config):
            raise Exception("uModules Configuration file [{0}] was not found.".format(args.config))

        ubom = open(args.config, "r")
        data = yaml.load(ubom)

        global project

        if "project" not in data:
            raise Exception("The configuration file is missing a [project] definition.")

        if "project_path" not in data:
            raise Exception("The configuration file is missing a [project_path] definition.")

        if "unity_path" not in data:
            raise Exception("The configuration file is missing a [unity_path] definition.")

        if "unity_packages_path" not in data:
            raise Exception("The configuration file is missing a [unity_packages_path] definition.")

        if "default_type" not in data:
            raise Exception("The configuration file is missing a [default_type] definition.")

        if "main" not in data:
            raise Exception("The configuration file is missing a [main] section.")

        if "modules" not in data:
            raise Exception("The configuration file is missing a [modules] section.")

        # create the project object
        project = Project(data["project"], data["project_path"], data["unity_path"], data["unity_packages_path"],
                          data["default_type"])

        # load main module information - this is our base project that we update with Unity
        # for the most part it is treated like a module
        proj_main = data["main"]

        # name is a required parameter
        if "name" not in proj_main:
            raise Exception("The configuration file is missing a [name] definition in the [main] section.")

        # url is a required parameter
        if "url" not in proj_main:
            raise Exception("The configuration file is missing a [url] definition in the [main] section.")

        # some default values for our main module
        _type = project.defaultType
        _branch = "master"

        # grab the required values
        _name = proj_main["name"]
        _url = proj_main["url"]

        if "branch" in proj_main:
            _branch = proj_main["branch"]

        if "type" in proj_main:
            _type = proj_main["type"]

        # create the main module
        project.main = MainModule(_name, _url, _branch, _type)

        if data["modules"]:

            for d in data["modules"]:

                # module is required
                if "module" not in d:
                    raise Exception("The [modules] section must contain a list of [module] items.")

                # url is required
                if "url" not in d:
                    raise Exception("The [module] {0} is missing a [url] definition.".format(d["module"]))

                # some module values
                _module = d["module"]
                _url = d["url"]
                _branch = "master"
                _type = project.defaultType
                _include = list()
                _include.append(Include())
                _active = True

                if "active" in d:
                    _active = d["active"]

                if "branch" in d:
                    _branch = d["branch"]

                if "type" in d:
                    _type = d["type"]

                if "includes" in d:

                    _include = []

                    for i in d["includes"]:

                        # dir is required
                        if "dir" not in i:
                            raise Exception("The [module] {0} [includes] section must contain [dir] list items.".format(
                                d["module"]))

                        _i = Include()
                        _dir = i["dir"]
                        _todir = _dir

                        # todir is optional
                        if "todir" in i:
                            _todir = i["todir"]
                        _i.dir = _dir
                        _i.todir = _todir
                        _include.append(_i)

                # ok lets create the module now
                module = Module(_module, _url, _branch, _type, _include, _active)

                # now we can update the project object with the module
                project.main.modules.append(module)

    except Exception as e:
        logger.error("unable to complete loading the configuration file")
        logger.error(e)
        raise Exception("Processing the Configuration File {0} was Stopped".format(args.config))


def load_vars():
    """Load Replacement Variables That Can Be Used for your Projects

    Sometimes it is desireable to not always have a static value for a
    variable in your uModules project. For example, the path to your
    Unity executeable might be different from one computer to the next.

    see the replace_var function for more information

    """
    global env
    env = {}

    try:

        logger.debug("attempting to open .umodules environment file from home path")

        um = os.path.expanduser("~") + "\\.umodules"
        logger.info(um)

        if not os.path.exists(um):
            logger.info(".umodules environment file was not found in the users home path")
            return

        env_file = open(um, "r")
        env_data = yaml.load(env_file)

        if "umodules" not in env_data:
            raise Exception("The .umodules Environment file must contain a [umodules] section.")

        data = env_data["umodules"]
        load_var_data(data)

        logger.debug("attempting to open .umodules environment file from project path")

        if not os.path.exists(".umodules"):
            logger.info(".umodules environment file was not found in the current project path")
            return

        env_file = open(".umodules", "r")
        env_data = yaml.load(env_file)

        if "umodules" not in env_data:
            raise Exception("The .umodules Environment file must contain a [umodules] section.")

        data = env_data["umodules"]
        load_var_data(data)

    except Exception as e:
        logger.error("unable to complete loading the .umodules environment file")
        logger.error(e)
        raise Exception("An unknown error occured trying to process the .umodules Environment file.")


def load_var_data(data):

        for key in data:
            env[key] = data[key]

        # env = data["umodules"]
        logger.debug(env)


def replace_var(var):
    """A Simple Function to Replace a String with an Environment Variable

    Environment Variables are defined in a file called .umodules which
    can be located in the current folder, or in your home folder.

    The file contains a list of key/values that can be used to replace
    configuration strings by surrounding the value with { }

    It is used mainly for setting your Unity path, but can be used for
    any configuratiojn setting.

    For example:

        unity_project_path: $unity_path_460$

    and your .umodules file contains

        umodules:
          unity_path_460 : /Development/Unity/Unity 4.6.0/Editor/

    will result with

        unity_project_path: /Development/Unity/Unity 4.6.0/Editor/

    :param var: the variable to return
    :return:
    """
    if len(env) == 0:
        return var

    if not var.startswith("$"):
        return var

    if not var.endswith("$"):
        return var

    v = var.replace("$", "")

    if v not in env:
        err = "Could not Find the Replacement Variable {0}".format(v)
        print(err)
        raise Exception(err)

    logger.debug("var in = {0}".format(var))
    ret = re.sub(r"\$(\w+?)\$", r"{\1}", var)
    ret = ret.format(**env)
    logger.debug("var out = {0}".format(ret))
    return ret


def new():

    filename = args.name.lower() + ".yml"

    try:
        if not os.path.exists(args.name):
            os.makedirs(args.name)
            os.makedirs(args.name + "/Assets")
            new_files(args.name)
        else:
            if not args.force:
                raise Exception("Project {0} already exists, use --force to overwrite".format(args.name))
            else:
                try:
                    os.remove(filename)
                    shutil.rmtree(args.name, onerror=remove_readonly)
                    new_files(args.name)
                except Exception as e:
                    raise e
    except Exception as e:
        print("Error creating new project {0}".format(args.name))
        raise e


def new_files(name):
    print(name)

    raw = '''\
# general information
project: {0}
project_path: .
unity_path: /Path/To/Unity/Editor/
unity_packages_path: /Path/To/Unity/Packages/
default_type: git

# main project
main: 

 name: {0}
 url: https://git@bitbucket.org/username/{1}.git
 branch: develop

# unity modules
modules:

- module: SmokeTestModule1
  url: https://git@bitbucket.org/spocker2/smoketestmodule1.git
  branch: develop
  includes:
  - dir: Assets/SmokeTestModule1
\
'''

    gitignore = '''\
# =============== #
# Unity generated #
# =============== #
Temp/
Library/

# ===================================== #
# Visual Studio / MonoDevelop generated #
# ===================================== #
ExportedObj/
obj/
*.svd
*.userprefs
/*.csproj
*.pidb
*.suo
/*.sln
*.user
*.unityproj
*.booproj

# ============ #
# OS generated #
# ============ #
.DS_Store
.DS_Store?
._*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db

# Ignore everything in Assets except for our Project Folder
Assets/*
!Assets/{0}
\
'''

    try:
        filename = name.lower() + ".yml"
        if not os.path.exists(filename):
            file = open(filename, 'w')
            a = collections.OrderedDict([(name, 1), (name.lower(), 2)])
            print(a)
            file.write(raw.format(*a))
            file.close()
    except Exception as e:
        raise e

    try:
        filename = "{0}/.gitignore".format(name)
        if not os.path.exists(filename):
            file = open(filename, 'w')
            a = collections.OrderedDict([(name, 1), (name.lower(), 2)])
            print(a)
            file.write(gitignore.format(*a))
            file.close()
    except Exception as e:
        raise e


def main(arguments):

    # setup logging
    global logger
    logger = logging.getLogger("umodules")
    logger.setLevel(logging.DEBUG)
    # output to logging file
    fh = logging.FileHandler("umodules.log", mode='w')
    fh.setLevel(logging.DEBUG)
    # output to stdout - for verbose
    ch = logging.StreamHandler()
    ch.setLevel(logging.CRITICAL)
    # formatters
    lf = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(lf)
    sf = logging.Formatter('%(message)s')
    ch.setFormatter(sf)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    logger.propagate = False

    # setup some global parms

    global args
    args = arguments

    global verbose
    verbose = args.verbose

    global force
    force = args.force

    global quiet
    quiet = args.quiet

    # do not allow quiet when we want to edit the project
    if args.command == "edit":
        quiet = False

    global modules
    modules = None
    opts = vars(args)
    if "modules" in opts:
        modules = opts["modules"]

    logger.info("uModules 0.1")
    logger.info("Copyright 2014 SpockerDotNet LLC")
    logger.debug(args)
    logger.debug(modules)

    # load the configuration files and execute
    try:
        load_vars()
        if args.command != "new":
            load_config()
        args.func()
    except Exception as e:
        print(e)
        logger.error(e)


if __name__ == '__main__':
    # create top level parsers
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest="config", help="use a different configuration file",
                        default="project.yml")
    parser.add_argument("-f", "--force", dest="force", action="store_true",
                        help="force command to execute when required", default=None)
    parser.add_argument("-q", "--quiet", dest="quiet", action="store_true",
                        help="for unity imports to be quiet", default=None)
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true",
                        help="show more information on console when running", default=None)
    subparsers = parser.add_subparsers(dest="command")
    subparsers.required = True

    # new - create a new uModules project in the current directory
    subnew = subparsers.add_parser("new", help="create a new uModules ready Unity project")
    subnew.set_defaults(func=new)
    subnew.add_argument("name", action="store")

    # clean - removes all subprojects
    subclean = subparsers.add_parser("clean", help="remove all modules from the project")
    subclean.set_defaults(func=clean)
    add_argument_modules(subclean)

    # cleanall - removes everything (currently dangerous)
    subcleanall = subparsers.add_parser("cleanall", help="remove the entire project")
    subcleanall.set_defaults(func=clean_all)
    add_argument_modules(subcleanall)

    # update - updates all sub-projects from repo (performs clean first)
    subupdate = subparsers.add_parser("update", help="refresh modules in the project")
    subupdate.set_defaults(func=update)
    add_argument_modules(subupdate)

    # push - update changes from project
    # subpush = subparsers.add_parser("push", help="push changes to repo")
    # subpush.set_defaults(func=push)
    # add_argument_modules(subpush)

    # pull - this will pull the main project and all subprojects
    subpull = subparsers.add_parser("pull", help="pull everything needed for the project")
    subpull.set_defaults(func=pull)
    add_argument_modules(subpull)

    # status - some information about this project
    subinfo = subparsers.add_parser("status", help="checks the state of this project")
    subinfo.set_defaults(func=status)

    # edit - open unity editor
    subbuild = subparsers.add_parser("edit", help="open this project in the Unity editor")
    subbuild.set_defaults(func=edit)

    # parse arguments and execute default function
    _args = parser.parse_args()
    main(_args)
